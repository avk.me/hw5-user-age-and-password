/* 
Ответ на теоретический вопрос:

Экранирование это способ превратить символ, используемый в js как специальный в обычный.
Для этого перед этим символом ставится обратный слэш "\".
*/

function createNewUser() {
    let firstName;
    let lastName;
    let birthDate;

    do{
        firstName = prompt("Enter your first name, please");
        lastName = prompt("Enter your last name, please");
        birthDate = prompt("Enter your birthday, please (dd.mm.yyyy)");
        
    }while(firstName === "" || lastName === "" || birthDate === "");
    
    const newUser = {
        firstName,
        lastName,
        birthDate,
                
        setFirstName(fName) {
            Object.defineProperty(this, "firstName", {
                value: fName,
            });
        },
        setLastName(lName) {
            Object.defineProperty(this, "lastName", {
                value: lName,
            });
        },
        getLogin() {
            let login = firstName.charAt(0) + lastName;
            return login.toLowerCase();
        },
        getAge() {
            const currentDate = new Date();
            const splitedBirthDayDate = this.birthDate.split(".");
            const reversedBirthDayDate = splitedBirthDayDate.reverse();
            for(elem in reversedBirthDayDate){
                reversedBirthDayDate[elem] = Number(reversedBirthDayDate[elem]);
            }
            const birthDayDate = new Date(reversedBirthDayDate);
            
            if(birthDayDate.getMonth() < currentDate.getMonth()){
                return currentDate.getFullYear() - birthDayDate.getFullYear();
            } else if (birthDayDate.getMonth() === currentDate.getMonth() && birthDayDate.getDate() < currentDate.getDate()){
                return currentDate.getFullYear() - birthDayDate.getFullYear();
            } else {
                return currentDate.getFullYear() - birthDayDate.getFullYear() - 1;
            }
        },
        getPassword() {
            const splitedBirthDayDate = this.birthDate.split(".");
            const password = firstName.charAt(0).toUpperCase() + lastName.toLowerCase() + splitedBirthDayDate[2];
            return password;
        },
    }
    

    Object.defineProperty(newUser, "firstName", {
        writable: false,
        configurable: true,
    });
    Object.defineProperty(newUser, "lastName", {
        writable: false,
        configurable: true,
    });
   
    return newUser;
}

const myUser = createNewUser();
console.log(myUser);
console.log(myUser.getAge());
console.log(myUser.getPassword());



